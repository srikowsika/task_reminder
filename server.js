
const Hapi = require('@hapi/hapi');
const nodemailer=require('nodemailer');
var routes = require('./routes/common_route');
var cron = require('node-cron')
var config=require('./config/config.json')
var sequelize=require('./controller/sequelize')

today=[]
cron.schedule('35 09 * * *', () => {
 
  sequelize.query(config.query.cron, function (error, results, fields) {
    if (error) throw error;
      mailHandler(results)
  });
 })

  async function mailHandler(today)
  {
      let transporter = nodemailer.createTransport({
        host: "smtp-mail.outlook.com",
        port: 587,
        secure: false, 
        auth: {
          user: config.usermail, 
          pass: config.password 
        }
      });

      today.map(task=>{
        
        let info = transporter.sendMail({
          from: '<srikowsika.guna@aspiresys.com>', 
          to: `${task.email}`,
          subject: `${task.name}- Reminder`, 
          text: `Task Name : ${task.name}`, 
          html:`<p>Your due date for the ${task.name} task assigned by ${task.assigned_by} is today</p>`
        });
        console.log("Message sent: %s", info.messageId);
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
      })  
  }
  const validate = async function (decoded, request, h) {
     await sequelize.query(config.query.userIsValid,{replacements:[decoded.id]})
      .then((user,m)=>{
      if(user.length){
        isValid={valid:'true'}
      }
      else{
        isValid={valid : 'false'}
      }
      })
  return {isValid};
};

const init = async () => {
  const server = Hapi.server({
    port: 5000,
    host: 'localhost',
    routes:{
        cors:{
            origin:['http://localhost:3000']
        }
    }
});
await server.register(require('hapi-auth-jwt2'));
  server.auth.strategy('jwt', 'jwt',
  { key: 'reminder_app', 
    validate  
  });
  server.auth.default('jwt');
  server.route(routes)
  server.start()
  return server;
}

init().then(()=>{
    console.log(`Server running on port 4000`);
})
.catch(err=>{
    console.log(err);
})



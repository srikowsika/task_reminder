const Sequelize = require('sequelize');
const config=require('../config/config.json')

const sequelize = new Sequelize(`${config.mysqlConnect.database}`, `${config.mysqlConnect.user}`, `${config.mysqlConnect.password}`, {
  host: `${config.mysqlConnect.host}`,
  dialect: 'mysql',
  logging:false
},{pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },define: {
    timestamps: false,
    logging:false
  }});
  sequelize.sync()
module.exports=sequelize;
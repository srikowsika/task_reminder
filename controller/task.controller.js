//var connection=require('./db')
const config=require('../config/config.json')
const Task=require('../models/task');
const sequelize =require('./sequelize')

getUserTask={
    handler: async (request,h)=>{
        //taskDetails= await new Promise((resolve,reject)=>{
            var taskDetails='';
            console.log(request.params.userId)
            await sequelize.query(config.query.userTask,{replacements:[request.params.userId],type: sequelize.QueryTypes.SELECT})
            .then((res,m)=>{
                taskDetails=res
            })
            .catch(err=>{
               // console.log(err)
            })
            console.log(taskDetails)
            return h.response(taskDetails).code(200)
        }
}

addTask={
    handler: async (request,h)=>{
        var addNewTask= ''
        await sequelize.query(config.query.addTask,{replacements:[request.payload.name,request.payload.due_date,request.payload.assigned_to,request.payload.assigned_by]})
            .then((res,m)=>{
                //console.log(res)
                addNewTask={
                    message:'Task added'
                }
            })
        return h.response(addNewTask).code(200) 
    }
}
module.exports={
    getUserTask,addTask
}
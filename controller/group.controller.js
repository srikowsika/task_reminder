// const connection = require('./db')
const config=require('../config/config.json')
// const Group=require('../models/group')
// const groupMember=require('../models/group_members')
const sequelize=require('./sequelize')

getGroupMembers={
    handler: async (request,h)=>{
        var response='';
    
        await sequelize.query(config.query.groupMembers,{replacements: [request.params.groupId]})
           .then(([res])=>{
               response=res
           })
           return h.response(response).code(200)
        }
        
        
}
 
// newGroup=async(groupName)=>{

//     return new Promise((resolve,reject)=>{
//         sequelize.query(config.query.createGroup,{replacements:[memberId,groupId]}) 
//         .then((res,m)=>{
//             resolve(res)
//         })
//            // if (error) reject(error);
            
       
//     })
// }

var addMember = async(groupId,memberArray) =>{
    console.log('groupid'+ groupId)
    return new Promise((resolve,reject)=>{
        console.log('groupid'+ groupId)
        memberArray.map(memberId=>{
            console.log('mem'+ memberId)
            sequelize.query(config.query.insertGroupMember,{replacements:[memberId,groupId]})
                .then((res,m)=>{
                    console.log(res)
                })
            })
        resolve (1);
    })
}

createGroup={
    handler: async (request,h)=>{
        var response=''
        var groupId=''
        var membersAdded=''
        await sequelize.query(config.query.createGroup,{replacements:[request.payload.groupName]} )
        .then(async (res,m)=>{
           // console.log('res')
            //console.log(res)
            groupId=res[0];
            await addMember(res[0],request.payload.groupMembers)
            .then(res=>{
                membersAdded=res
            })
        })
        .catch(err=>{
            console.log(err)
        })
        if(membersAdded==1)
        {
            response={
                statusMessage:'Group Created',
                groupName:request.payload.groupName,
                groupId:groupId
            }
        } 
        console.log(response)
        return(h.response(response).code(200))
    }
}

getGroupsOfAUser={
    handler: async (request,h)=>{
        var groupDetails='';
        //console.log('fdksj')
        await sequelize.query(config.query.groupsOfAUser,{replacements:[request.params.userId],type: sequelize.QueryTypes.SELECT})
        .then((res,m)=>{
            console.log(m)
            groupDetails=res;
        })
        return h.response(groupDetails).code(200)
    }  
}
module.exports={
    getGroupsOfAUser,
    getGroupMembers,
    createGroup
}



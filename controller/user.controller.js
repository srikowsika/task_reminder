
const config=require('../config/config.json')
const jwt=require('hapi-jsonwebtoken')
const jwtConfig=require('../auth')
const sequelize =require('./sequelize')

getUsers={
    
    handler: async (request,h)=>{
            var userDetails=''
            await sequelize.query(config.query.getUser )
            .then(([res,m])=>{
                userDetails=res;
            })
            return(h.response(userDetails))
    }  
}


getUsersByManagerId={
    handler: async (request,h)=>{
         var userDetails=''
        await sequelize.query(config.query.userUnderAManager,{replacements: [request.params.managerId],type: sequelize.QueryTypes.SELECT})
        .then(res=> {
            userDetails=res
            })
        return(h.response(userDetails))
    }  
}

validate = async (user) => {
       
    return new Promise((resolve,reject)=>{
            console.log(user.length)
            if(!user.length)
            {
                response={ 
                    statusMessage:'Incorrect username or password',
                    credentials: null, 
                    isValid: false 
                } 
                reject(response);
            }
            else
            {
                const isValid = true;
                const credentials = { id: user[0].id, name: user[0].first_name+' '+user[0].last_name, role:user[0].role };
                token = jwt.sign({ id: credentials.id, username: credentials.name, scope: credentials.role},jwtConfig.auth, { algorithm: 'HS256', expiresIn: "1h" })
                response={statusMessage:'Login Successful',isValid, credentials, token }
                resolve(response);
            }
             
        } )
       
}                

userLogin={
    handler: async (request,h)=>{
         var loginDetails=''
         var code;
            await sequelize.query(config.query.userLogin,{replacements:[request.payload.email,request.payload.password],type: sequelize.QueryTypes.SELECT}) 
            .then(async (res,m)=>{
                await validate(res)
                .then(res=>{
                   
                   loginDetails=res
                   code=200
                })
                .catch(res=>{
                    console.log('err')
                    loginDetails=res
                    code=401;
                })
            })
            return (h.response(loginDetails).code(code))
                
    }
}


module.exports={
    getUsers,
    getUsersByManagerId,
    userLogin
}



import React from 'react';
import RouterComponent from './router/route'
import {BrowserRouter} from 'react-router-dom';
import NavBar from './component/navBar'

function App() {
  return (
    <>
        <BrowserRouter>
        
           <RouterComponent/>
           
        </BrowserRouter>
    </>
  );
}

export default App;

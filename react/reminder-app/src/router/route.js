import React from 'react';
import {Route, Switch} from 'react-router-dom';
import {TaskContainer} from '../container/taskContainer'
import addTask from '../container/addTaskContainer'
import signin from '../container/signinCointainer'
import createGroup from '../container/createGroupCointainer'
import group from '../container/groupCointainer'
import home from '../component/home'
import groupFunction from '../container/groupFunctionalityContainer'
import axios from 'axios'
import viewMembers from '../container/manager'
import {Redirect} from 'react-router-dom'
import NavBar from '../component/navBar'

axios.interceptors.request.use(function (config) {
    var token=localStorage.getItem('token')
    if ( token != null ) {
        config.headers.Authorization = `${token}`;
    }
    return config;
  }, function (error) {
    return Promise.reject(error);
  });

const RouterComponent=()=>{

    const ManagerRoute = ({ component: Component, ...rest }) => (
        <Route {...rest} render={(props) => (
          localStorage.getItem("token") !== null ? localStorage.getItem("role") === 'manager' ? <Component {...props} />
            : <Redirect to='/home' />
            : <Redirect exact to='/' />
        )} />
      )
      const PrivateRoute = ({ component: Component, ...rest }) => (
        <Route {...rest} render={(props) => (
          localStorage.getItem("token") !== null?
            <Component {...props} />
            : <Redirect to='/' />
        )} />)

   
    return(
        <>
            <Switch>
                <Route exact path='/' component={signin}></Route>
                <PrivateRoute path='/home' component={home}></PrivateRoute>
                <PrivateRoute exact path='/task' component={TaskContainer}></PrivateRoute>
                <PrivateRoute path='/task/addTask' component={addTask}></PrivateRoute>
                <PrivateRoute exact path='/groups' component={group}></PrivateRoute>
                <PrivateRoute  path='/group/:groupId/:groupname' component={groupFunction}></PrivateRoute>
                <ManagerRoute  path='/members' component={viewMembers}></ManagerRoute>
                <PrivateRoute path='/group/create' component={createGroup}></PrivateRoute>
                <Route render={()=>(<h1>404 Not Found</h1>)} />
            </Switch>
            
        </>
    )
}
export default RouterComponent


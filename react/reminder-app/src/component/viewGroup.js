import React from 'react';
import {Link} from 'react-router-dom'
 
 const ViewGroup=(props)=>{
  
    return(
        <div id="div">
        <table className="table table-striped table-responsive " id='table'>

          <thead className="thead-light">
            <tr>
              <th scope="col">Group Name</th>
            </tr>
          </thead>
          </table>
          
            {
              props.groupDetails.map(group=>(
               
                <ul key={group.group_id}>
                
                <Link to={`/group/${group.group_id}/${group.group_name}`} onClick={(e)=>props.groupDetail}>{group.group_name}</Link>
                </ul>
              ))
            }
          
        </div>
        )}
export default ViewGroup;
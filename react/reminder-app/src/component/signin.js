import React from 'react';
import { Button, Form, FormGroup, Label, Input, Card,CardTitle} from 'reactstrap';
import '../css/stylesheet.css'
 
 const Signin=(props)=>{
        
    return(
        <div id="div">
         <Card id="card">
         <CardTitle>Signin Form</CardTitle>
         {props.isLogin === false? <p>Incorrect username r password</p>:null}
         <Form onSubmit={(e)=>props.submitHandler(e)}>
                <FormGroup>
                        <Label>Email </Label>
                        <Input type='email' className='input' name='email' onChange={(e)=>props.handleChange(e)}required />
                </FormGroup>
                
                <FormGroup>
                        <Label>Password</Label>
                        <Input type='password' className='input' name="password"  onChange={(e)=>props.handleChange(e)}  required />
                </FormGroup>
                <Button type='submit'>Signin</Button>
         </Form>
         </Card>
        </div>
        )}
export default Signin;
import React from 'react';
import { Nav, NavItem} from 'reactstrap';
import {NavLink} from 'react-router-dom';

const NavBar = (props) => {
  // console.log('dkj')
  return (
    <div>
     
      <Nav vertical>
        <NavItem>
        <NavLink
          to="/task"
          activeStyle={{
            fontWeight: "bold",
            color: "red"
          }}
        >
          Task
        </NavLink>
        </NavItem>
        <NavItem>
          <NavLink 
          to="/groups"
          activeStyle={{
            fontWeight: "bold",
            color: "red"
          }}>Group</NavLink>
        </NavItem>
        <NavItem>
           {localStorage.getItem('role')=== 'manager'?
            <NavLink 
            to="/members"
            activeStyle={{
              fontWeight: "bold",
              color: "red"
            }}>Members</NavLink>:null}
        </NavItem>
        <NavItem>
           {localStorage.getItem('token')!== null?
            <NavLink 
            to="/"
            onClick={()=>localStorage.clear()}>Logout</NavLink>:null}
        </NavItem>
      </Nav> 
    </div>
  );
}

export default NavBar;
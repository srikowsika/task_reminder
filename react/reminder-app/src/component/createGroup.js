import React from 'react';
import { Multiselect } from 'multiselect-react-dropdown';
import { Button, Form, FormGroup, Label, Input, Card,CardTitle} from 'reactstrap';
import '../css/stylesheet.css'

const CreateGroup=(props)=>{
        var option=[]
        props.users.map(user=>{
            var options={
                label : user.first_name+' '+user.last_name,
                value : user.id
            }
            option.push(options);
            return option
        })
        
    return(
        
        <div id="div">
            {props.created === false? <p>Group name already exists</p>:null}
            <Card id="card">
                <CardTitle>Create Group</CardTitle>
                    <Form onSubmit={props.submitHandler}>
                        <FormGroup>
                            <Label>Group Name</Label>
                            <Input name='groupName' onChange={(e)=> props.changeEvent(e)} required></Input>
                        </FormGroup>
                        <FormGroup>
                            <Label>Group Members</Label>
                            <Multiselect 
                            options={option}
                            displayValue="label" 
                            onSelect={props.selectHandler}
                            onRemove={props.removeHandler}
                            />
                        </FormGroup>
                        <Button type='submit' >Create</Button>  
                    </Form>
            </Card>
        </div>
    )

}

export default CreateGroup;
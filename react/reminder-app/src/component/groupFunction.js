import React from 'react';
// import {Link} from 'react-router-dom'
import Members from './member'
import AddTask from './addTask';
 
const GroupFunctions=(props)=>{
    return(
        <div id="div">
           <p>Group Name: {props.groupName}</p> 
           <p>Members</p>
            <Members groupMembers={props.members} />
            {/* <Link to='/'>Add Members</Link><legend></legend> */}
            {/* <Link to={location => ({ ...location, pathname: `${location.pathname}/assign/task`})}>Assign Task</Link> */}
            <AddTask members={props.members} handleChange={props.handleChange} submitHandler={props.submitHandler} path={props.path}/>
            
        </div>
        )}
export default GroupFunctions;
import React from 'react';

 
 const ViewMembers=(props)=>{
  
    return(
        <div id="div">
        <table className="table table-striped table-responsive " id='table'>

          <thead className="thead-light">
            <tr>
              <th scope="col">Group Name</th>
            </tr>
          </thead>
          <tbody>
            {
              props.groupDetails.map(group=>(
                <tr key={group.group_id} >
                <td>{group.group_name}</td>
              </tr>
              ))
            }
          </tbody>
        </table>
        </div>
        )}
export default ViewMembers;
import React from 'react';
import {Link} from 'react-router-dom'


 const Home=()=>{
    return(
        <div id="div">
            <Link to='/task'>Task</Link>
            <legend></legend>
            <Link to='/groups'>Group</Link>
            <legend></legend>
            {localStorage.getItem('role')=== '1'?
            <Link to='/members'>View Members</Link>:null}
        </div>)
}
export default Home;
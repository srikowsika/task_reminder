import React from 'react';
import { Button, Form, FormGroup, Label, Input, Card,CardTitle} from 'reactstrap';
import '../css/stylesheet.css'

 
 const AddTask=(props)=>{
        var date=new Date()
        date=date.toISOString().slice(0,10);
    return(
        <div id="div">
         <Card id="card">
         <CardTitle>Create Task</CardTitle>
         <Form onSubmit={(e)=>props.submitHandler(e)}>
                <FormGroup>
                        <Label>Task Name </Label>
                        <Input placeholder='Enter the task name' className='input' name='taskName' onChange={(e)=>props.handleChange(e)}required />
                </FormGroup>
                {props.path  === "/task/addTask"? null:
                <FormGroup>
                <Label>Assigned to</Label>
                        <select type='number' name='assignedTo' onChange={(e)=>props.handleChange(e)} required>
                                <option></option>
                        { props.members.map(member=>(
                                <option value={member.user_id} key={member.user_id}>{member.first_name+' '+member.last_name}</option>
                                ))
                        } 
                        </select>
                </FormGroup>}
                <FormGroup>
                        <Label>Due Date</Label>
                        <Input label='Due Date' type="date" className='input' name="dueDate" min={date} onChange={(e)=>props.handleChange(e)} placeholder="DD-MM-YYYY" required />
                </FormGroup>
                <Button type='submit'>Add</Button>
         </Form>
         </Card>
        </div>
        )}
export default AddTask;
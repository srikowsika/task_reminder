import React from 'react';
import {Link} from 'react-router-dom'
import ViewGroup from '../container/viewGroupCointainer';
import NavBar from './navBar'
 const Group=()=>{
    return(
        <div>
            <NavBar />
            <ViewGroup />
            <Link to='/group/create'>New Group</Link>
        </div>)
}
export default Group;
import React from 'react';
import {Link} from 'react-router-dom'
import ViewTask from '../container/viewTaskContainer';
import NavBar from '../component/navBar'
 
const Task=()=>{
    return(
        <div>
            <NavBar />
            <ViewTask />
            <Link to='/task/addTask'>Add Task</Link>
        </div>)
}
export default Task;
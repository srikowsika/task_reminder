import React from 'react';

 
 const ViewTask=(props)=>{

    // var dateConvertor=(due_date)=>{
    //   due_date=new Date(due_date)
    //   var date=due_date.toISOString().slice(0,10);
    //   return date
    // }
    return(
        <div id="div">
        <table className="table table-striped table-responsive " id='table'>

          <thead className="thead-light">
            <tr>
              <th scope="col">Task Name</th>
              <th scope="col">Due Date</th>
              <th scope="col">Assigned by</th>
            </tr>
          </thead>
          <tbody>
            {
              props.task.map(task=>(
                
                <tr key={task.id} >
                <td>{task.name}</td>
                <td>{task.due_date}</td>
                <td>{task.first_name+' '+task.last_name}</td>
              </tr>
              ))
            }
          </tbody>
        </table>
        </div>
        )}
export default ViewTask;

import React from 'react';
// import {Link} from 'react-router-dom'
// import ViewGroup from '../container/viewGroupCointainer';

 const Members=(props)=>{
    return(
        <div>
           {
                props.groupMembers.map(member=>(
                <ul key={member.user_id}>
                    <li>{member.first_name+' '+member.last_name}</li>
                </ul>
                ))
            }
        </div>)
    
}
export default Members;
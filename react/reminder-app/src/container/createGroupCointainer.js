import React,{Component} from 'react';
import CreateGroup from '../component/createGroup'
import axios from 'axios';
import config from '../config.json'

class CreateGroupContainer extends Component{
    constructor()
    {
        super()
        this.state={
            groupName:'',
            groupMembers:[],
            users:{},
            dataLoaded:false,
            members:'',
            created:''
            
        }
    }

    componentDidMount()
    {
        this.getUsers()
    }

    getUsers = () =>{
        axios.get(`${config.url}/users`)
        .then(res=>{
            this.setState({
                users:res.data,
                dataLoaded:true,      
            })
        })
        .catch(err=>{
            console.log(err)
        })
    }

    createGroup = (e) =>{
        e.preventDefault() 
        var groupDetails={
            groupName:this.state.groupName,
            groupMembers:this.state.groupMembers
        }
        axios.post(`${config.url}/groups/create`,groupDetails)
        .then(res=>{
            this.props.history.push({
                pathname: '/groups',
            })
        })
        .catch(err=>{
            console.log(err)
        })
    }

    changeEvent= (e) => {
        console.log(e.target.value)
        this.setState({ [e.target.name]:e.target.value})
     }

     selectHandler=(selectedList, selectedItem)=>{
         this.setState(prevState => ({
            groupMembers: [...prevState.groupMembers, selectedItem.value]
          }))
     }

     removeHandler=(selectedList, removedItem)=>{
        const members = this.state.groupMembers.filter(member => member !== removedItem.value);
        this.setState({ groupMembers : members})
     }

    render(){
        
        return(
            <>
                {
                    this.state.dataLoaded && 
                    <CreateGroup users={this.state.users} changeEvent={this.changeEvent} submitHandler={this.createGroup} created={this.state.created} selectHandler={this.selectHandler} removeHandler={this.removeHandler}/>
                }
            </>
        )
    }
}

export default CreateGroupContainer;
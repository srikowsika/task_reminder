import React,{Component} from 'react';
import axios from 'axios';
import config from '../config.json'
import AddTask from '../component/addTask';


class AddTaskCointainer extends Component{

    constructor(props)
    {
        super()
        this.state={
            members:{},
            dataLoaded: false,
            dueDate:'',
            assignedTo:localStorage.getItem('userId'),
            taskName:'',
            userId:localStorage.getItem('userId')
        }
    }

    changeEvent= (e) => {
        this.setState({ [e.target.name]:e.target.value})
     }

     submitHandler =(e) =>{
        e.preventDefault()
        var taskDetails={
            name:this.state.taskName,
            due_date:this.state.dueDate,
            assigned_to:parseInt(this.state.assignedTo),
            assigned_by:parseInt(this.state.userId)
        }
        console.log(taskDetails)
        axios.post(`${config.url}/task/add`,taskDetails)
        .then(res=>{
            
            this.props.history.push({
                pathname: '/task',
            })
        })
        .catch(err=>{
            console.log(err)
        });    
     }

    render()
    {
        return(
            <>
            {
                <AddTask members={this.state.members} handleChange={this.changeEvent} submitHandler={this.submitHandler} path={this.props.location.pathname} assignedTo={this.state.assignedTo}/>
            }
            </>
        )
    }
}

export default AddTaskCointainer;
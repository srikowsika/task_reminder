import React,{Component} from 'react';
import axios from 'axios';
import config from '../config.json'
import ViewTask from '../component/viewTask';


class viewTaskCointainer extends Component{

    constructor()
    {
        super()
        this.state={
            taskDetails:'',
            userId:localStorage.getItem('userId')
        }
    }

    componentDidMount()
    {
        this.getTask()
    }

    getTask = ()=>{
        axios.get(`${config.url}/task/${this.state.userId}`)
        .then(res=>{
            //console.log(res.data)
            this.setState({
                taskDetails:res.data,
                dataLoaded:true,
            })
        })
    }

    render()
    {
        return(
            <>
            {
                this.state.dataLoaded&&
                <ViewTask task={this.state.taskDetails}/>
            }
            </>
        )
    }
}

export default viewTaskCointainer;
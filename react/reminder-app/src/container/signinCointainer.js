import React,{Component} from 'react';
import axios from 'axios';
import config from '../config.json'
import Signin from '../component/signin';
import {Redirect} from 'react-router-dom'


class SigninCointainer extends Component{

    constructor(props)
    {
        super()
        this.state={
            email:'',
            password:'',
            isLogin:''
        }
    }

    changeEvent= (e) => {
       
        this.setState({ [e.target.name]:e.target.value})
     }

     submitHandler = (e) =>{
        e.preventDefault()
        var userDetails={
            email:this.state.email,
            password:this.state.password
        }
        axios.post(`${config.url}/user/signin`,userDetails)
        .then(res=>{
            if(res.status===200)
            {
                localStorage.setItem('userId',res.data.credentials.id);
                localStorage.setItem('token',res.data.token)
                localStorage.setItem('role',res.data.credentials.role)
                this.props.history.push({
                    pathname: '/task',
                })
            }  
            else{
                this.setState({
                    isLogin:false
                })
                // this.props.history.push({
                //     pathname: '/',
                // })
            }
        })
        .catch(err=>{
            console.log(err)
        });    
     }

    render()
    {
        return(
            <>
           
                <Signin handleChange={this.changeEvent} submitHandler={this.submitHandler} isLogin={this.state.isLogin}/>
            
            </>
        )
    }
}

export default SigninCointainer;


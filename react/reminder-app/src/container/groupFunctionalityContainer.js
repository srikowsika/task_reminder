import React,{Component} from 'react';
import axios from 'axios';
import config from '../config.json'
import GroupFunction from '../component/groupFunction';


class GroupFunctionContainer extends Component{

    constructor(props)
    {
        super()
        this.state={
            members:{},
            groupId:props.match.params.groupId,
            groupName:props.match.params.groupname,
            dueDate:'',
            assignedTo:localStorage.getItem('userId'),
            taskName:'',
            userId:localStorage.getItem('userId')
        }
    }

    componentDidMount()
    {
        this.getMembers()
    }

    getMembers = ()=>{
        axios.get(`${config.url}/groups/users/${this.state.groupId}`)
        .then(res=>{
            //console.log(res.data)
            this.setState({
                members:res.data,
                dataLoaded:true,

            })
        })
    }

    
     changeEvent= (e) => {
        //console.log(e.target.value)
        //console.log(typeof(e.target.value))
        this.setState({ [e.target.name]:e.target.value})
     }

     submitHandler =(e) =>{
        e.preventDefault()
        var taskDetails={
            name:this.state.taskName,
            due_date:this.state.dueDate,
            assigned_to:parseInt(this.state.assignedTo),
            assigned_by:parseInt(this.state.userId)
        }
        console.log(taskDetails)
        axios.post(`${config.url}/task/add`,taskDetails)
        .then(res=>{
            console.log(res)
            this.props.history.push({
                pathname: '/groups',
            })
        })
        .catch(err=>{
            console.log(err)
        });    
     }

    render()
    {
        return(
            <>
            {
                this.state.dataLoaded &&
                <GroupFunction members={this.state.members} groupName={this.state.groupName} handleChange={this.changeEvent} submitHandler={this.submitHandler} path={this.props.location.pathname} assignedTo={this.assignedTo} path={this.props.location.pathname}/>
            }
            </>
        )
    }
}

export default GroupFunctionContainer;
import React,{Component} from 'react';
// import Group from '../component/group';
import Members from '../component/member'
import axios from 'axios';
import config from '../config.json'
import AddTask from '../component/addTask';

export default class Manager extends Component{
    constructor(props)
    {
        super(props)
        this.state={
            members:{},
            userId:localStorage.getItem('userId'),
            dataLoaded:false
        }
    }
    componentDidMount()
    {
        axios.get(`${config.url}/users/${this.state.userId}`)
        .then(res=>{
            console.log(res)
            this.setState({
                members:res.data,
                dataLoaded:true
            })
        })
        .catch(err=>{
            console.log(err)
        })
    }
    render()
    {
        console.log(this.state.members)
        return (
            
                <>
                {
                    this.state.dataLoaded&&
                    <>
                        <Members groupMembers={this.state.members}/>
                        <AddTask members={this.state.members}/>
                    </>
                    // <AddTask members={this.state.members} handleChange={this.changeEvent} submitHandler={this.submitHandler} path={this.props.location.pathname} assignedTo={this.assignedTo}/>
                }
                    
                </>
            
            );
           
        
    }
}


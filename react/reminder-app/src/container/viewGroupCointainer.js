import React,{Component} from 'react';
import axios from 'axios';
import config from '../config.json'
import ViewGroup from '../component/viewGroup';


class viewGroupCointainer extends Component{

    constructor()
    {
        super()
        this.state={
            groupDetails:'',
            userId:localStorage.getItem('userId')
        }
    }

    componentDidMount()
    {
        this.getGroup()
    }

    getGroup = ()=>{
        axios.get(`${config.url}/groups/${this.state.userId}`)
        .then(res=>{
           // console.log(res.data)
            this.setState({
                groupDetails:res.data,
                dataLoaded:true,
            })
        })
    }
    getGroupDetail=(e)=>{
        console.log(e)
    }
    render()
    {
        return(
            <>
            {
                this.state.dataLoaded&&
                <ViewGroup groupDetails={this.state.groupDetails} groupDetail={this.getGroupDetail}/>
            }
            </>
        )
    }
}

export default viewGroupCointainer;
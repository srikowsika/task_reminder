
const sequelize=require('../controller/sequelize')
const Sequelize=require('sequelize')

const GroupMembers = sequelize.define('group_members', {
    userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      groupId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'group_details',
          key: 'id'
        }
      }
    },{
        tableName: 'group_members',
           timestamps:false,
           underscored: true,
      })
 
module.exports=GroupMembers;
    
const sequelize=require('../controller/sequelize')
const Sequelize=require('sequelize')

const User = sequelize.define('user', {
    // attributes
    firstName: {
      type: Sequelize.STRING,
      allowNull: false
    },
    lastName: {
      type: Sequelize.STRING,
      allowNull:false
    },
    email:{
        type: Sequelize.STRING,
        allowNull:false
    },
    role:{
        type: Sequelize.STRING,
        allowNull:false
    },
    manager_id:{
        type: Sequelize.INTEGER,
        allowNull:true
    },
    password:{
        type: Sequelize.STRING,
        allowNull:false
    }

  }, {
       modelName: 'user',
       timestamps:false,
       underscored: true,
  });

module.exports=User;
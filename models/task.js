
const sequelize=require('../controller/sequelize')
const Sequelize=require('sequelize')

const Task = sequelize.define('task', {
    name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      due_date:{
          type:Sequelize.DATEONLY,
          allowNull:false
      },
      assigned_to: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      assigned_by: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id'
        }
      }
    },{
        tableName: 'task',
           timestamps:false,
           underscored: true,
      })
 
module.exports=Task;
    
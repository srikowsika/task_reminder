const sequelize=require('../controller/sequelize')
const Sequelize=require('sequelize')
const Model = Sequelize.Model;

const Group = sequelize.define('group_detail', {
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    
  }, {
    tableName: 'group_details',
       timestamps:false,
       underscored: true,
  });
 
module.exports=Group;

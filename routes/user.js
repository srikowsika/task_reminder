var userController=require('../controller/user.controller')
var auth=require('../auth')

const userRoute = [
    { 
        method: 'GET', 
        path: '/users', 
        config: {
            handler:userController.getUsers.handler,
           auth: {
               scope:['manager','employee']
           }
        }
    },
    {
        method: 'GET',
        path: '/users/{managerId}',
        config:{
            handler:userController.getUsersByManagerId.handler,
            auth: {
                scope:['manager']
            }
        },
    },
    { 
        method: 'POST', 
        path: '/user/signin', 
        config: {
            handler:userController.userLogin.handler,
            auth:false
        }
    },

    ];  
      
module.exports=userRoute;
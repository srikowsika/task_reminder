var taskController=require('../controller/task.controller')
var auth=require('../auth')

module.exports = [
    { 
        method: 'GET', 
        path: '/task/{userId}', 
        config: {
            handler:taskController.getUserTask.handler ,
            auth: {
                scope:['manager','employee']
            }
        }
    },
    {
        method: 'POST', 
        path: '/task/add', 
        config: {
            handler:taskController.addTask.handler,
            auth: {
                scope:['manager','employee']
            }
        }
    }
    ];
var groupController=require('../controller/group.controller')
var auth=require('../auth')

module.exports = [
    {
        method:'GET' ,
        path:'/groups/{userId}' , 
        config:{
            handler:groupController.getGroupsOfAUser.handler,
            auth: {
                scope:['manager','employee']
            }
        } 
    },
    { 
        method: 'GET', 
        path: '/groups/users/{groupId}', 
        config: {
            handler:groupController.getGroupMembers.handler,
            auth: {
                scope:['manager','employee']
            }
        }
    },
    {
        method:'POST',
        path: '/groups/create', 
        config: {
            handler:groupController.createGroup.handler,
            auth: {
                scope:['manager','employee']
            }
        }
    }
];